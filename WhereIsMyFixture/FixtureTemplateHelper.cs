﻿using System.IO;
using System.Linq;

namespace WhereIsMyFixture
{
    public static class FixtureTemplateHelper
    {
        private static string GetActiveClassNamespace(string classPath)
        {
            return File.ReadAllLines(classPath)
                .First(x => x.Contains("namespace"))
                .Split(' ')
                .Skip(1).First()
                .Trim(' ', '{');
        }

        /// <summary>
        /// Gets the fixture class template.
        /// </summary>
        /// <param name="classPath">The class path.</param>
        public static string GetFixtureClassTemplate(string classPath)
        {
            string template = Resource.FixtureTemplate;

            template = template.Replace("##NAMESPACE##", GetActiveClassNamespace(classPath));
            template = template.Replace("##CLASSNAME##", PathsHelper.GetClassName(classPath));

            return template;
        }
    }
}
